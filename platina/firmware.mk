# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi-firmware/platina/abl.elf:install/firmware-update/abl.elf \
    vendor/xiaomi-firmware/platina/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/xiaomi-firmware/platina/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/xiaomi-firmware/platina/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/xiaomi-firmware/platina/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/xiaomi-firmware/platina/dspso.bin:install/firmware-update/dspso.bin \
    vendor/xiaomi-firmware/platina/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/xiaomi-firmware/platina/keymaster64.mbn:install/firmware-update/keymaster64.mbn \
    vendor/xiaomi-firmware/platina/logfs_ufs_8mb.bin:install/firmware-update/logfs_ufs_8mb.bin \
    vendor/xiaomi-firmware/platina/logo.img:install/firmware-update/logo.img \
    vendor/xiaomi-firmware/platina/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/xiaomi-firmware/platina/pmic.elf:install/firmware-update/pmic.elf \
    vendor/xiaomi-firmware/platina/rpm.mbn:install/firmware-update/rpm.mbn \
    vendor/xiaomi-firmware/platina/storsec.mbn:install/firmware-update/storsec.mbn \
    vendor/xiaomi-firmware/platina/tz.mbn:install/firmware-update/tz.mbn \
    vendor/xiaomi-firmware/platina/xbl.elf:install/firmware-update/xbl.elf
